package ch.swissbytes.todo;

import org.hamcrest.Matchers;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

// selenium docs: http://docs.seleniumhq.org/docs/03_webdriver.jsp
public class AmazonTest {
    private static WebDriver driver;


   // @BeforeClass
    public static void setup() throws InterruptedException {
        driver = new FirefoxDriver();
    }

  //  @Test
    public void testAddingAndCompletingItems() throws InterruptedException{
        String title = "My Item title";
        String description = "My item description";

        driver.get("http://www.amazon.com/");

        buscarIphone6();

    }

    private WebElement searchTagById(String id){
        WebElement webElement = driver.findElement(By.id(id));
        assertThat(webElement, notNullValue());
        return webElement;
    }

    private void buscarIphone6(){
        WebElement webElement  =  searchTagById("twotabsearchtextbox");
        webElement.sendKeys("iphone 6 plus");
        webElement.submit();
    }



}
