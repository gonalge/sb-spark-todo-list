package ch.swissbytes.todo;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

// selenium docs: http://docs.seleniumhq.org/docs/03_webdriver.jsp
public class ToDoListAcceptanceTestV1 {
    private static WebDriver driver;
    private static final ToDoList app = new ToDoList();

    @BeforeClass
    public static void setup() throws InterruptedException {
       app.startup(new EntityManager());
       Thread.sleep(700);//let's give it some time to start the server-socket, for slower machines

        driver = new FirefoxDriver();
    }

    @AfterClass
    public static void tearDown(){
        app.shutdown();
    }

    @Test
    public void willListTodoItems() {
        String title = "My Item title";
        String description = "My item description";

        driver.get("http://localhost:4567/items");
        assertThat(driver.findElement(By.cssSelector("body")).getText(), containsString("My ToDo List"));

        WebElement newItemLink = driver.findElement(By.linkText("New Item"));
        newItemLink.click();
        assertThat(driver.findElement(By.cssSelector("body")).getText(), containsString("Create new item"));

        WebElement titleField = driver.findElement(By.name("title"));
        WebElement descriptionField = driver.findElement(By.name("description"));
        titleField.sendKeys(title);
        descriptionField.sendKeys(description);
        titleField.submit();

        assertThat(driver.findElement(By.cssSelector("body")).getText(), containsString("Item added successfully!!"));
        WebElement backLink = driver.findElement(By.linkText("Back to list"));
        backLink.click();

        assertThat(driver.findElement(By.cssSelector("body")).getText(), containsString("My ToDo List"));
        List<WebElement> items = driver.findElements(By.cssSelector("#items li"));
        WebElement newItem = null;
        for (WebElement item : items) {
            if (item.getText().contains(title)) {
               newItem = item;
            }
        }
        WebElement viewLink = newItem.findElement(By.className("view"));

        viewLink.click();
        assertThat(driver.findElement(By.cssSelector("body")).getText(), containsString(title));
        assertThat(driver.findElement(By.cssSelector("body")).getText(), containsString(description));
        backLink = driver.findElement(By.linkText("Back to list"));
        backLink.click();

        assertThat(driver.findElement(By.cssSelector("body")).getText(), containsString("My ToDo List"));
        items = driver.findElements(By.cssSelector("#items li"));
        newItem = null;
        for (WebElement item : items) {
            if (item.getText().contains(title)) {
                newItem = item;
            }
        }
        WebElement completeLink = newItem.findElement(By.className("complete"));
        completeLink.click();

        items = driver.findElements(By.cssSelector("#items li"));
        newItem = null;
        for (WebElement item : items) {
            if (item.getText().contains(title)) {
                newItem = item;
            }
        }
        assertThat(newItem, nullValue());

        items = driver.findElements(By.cssSelector("#completed-items li"));
        WebElement completedItem = null;
        for (WebElement item : items) {
            if (item.getText().contains(title)) {
                completedItem = item;
            }
        }
        assertThat(completedItem, notNullValue());
    }
}
